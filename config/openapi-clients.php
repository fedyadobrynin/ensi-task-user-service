<?php

return [
    'posts' => [
        'client' => [
            'base_uri' => env('POSTS_CLIENT_SERVICE_HOST') . "/api/v1",
        ]
    ],
];

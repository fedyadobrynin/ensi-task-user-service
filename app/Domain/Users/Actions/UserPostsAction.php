<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;
use Ensi\BackendServiceSkeletonClient\Api\PostApi;
use Ensi\BackendServiceSkeletonClient\Dto\SearchPostsRequest;

class UserPostsAction
{
    public function __construct(protected PostApi $postApi)
    {
    }

    public function execute(int $userId): int
    {
        User::findOrFail($userId);
        $search_posts_request = new SearchPostsRequest(['filter' => ['user_id' => $userId]]);
        return $this->postApi->searchPosts($search_posts_request)->getMeta()['pagination']['total'];
    }
}

<?php

namespace App\Console\Commands\Users;

use App\Domain\Users\Actions\UserPostsAction;
use Illuminate\Console\Command;

class CountUserPostsCommand extends Command
{
    protected $signature = 'users:get-posts-count {user_id}';
    protected $description = 'Получить общее число постов, созданных указанным пользователем';

    public function handle(UserPostsAction $action)
    {
        $userId = $this->argument('user_id');
        $postsCount = $action->execute($userId);
        $this->info("Число постов пользователя: {$postsCount}");
    }
}
